#!/usr/bin/env bash

    L='tr' && sudo sed -i 's/XKBLAYOUT=\"\w*"/XKBLAYOUT=\"'$L'\"/g' /etc/default/keyboard
    echo "Europe/Istanbul" > /etc/timezone
    dpkg-reconfigure -f noninteractive tzdata
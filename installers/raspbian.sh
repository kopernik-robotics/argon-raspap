UPDATE_URL="https://bitbucket.org/kopernik-robotics/argon-raspap/raw/master"
wget -q ${UPDATE_URL}/installers/common.sh -O /tmp/raspapcommon.sh
source /tmp/raspapcommon.sh && rm -f /tmp/raspapcommon.sh

function update_system_packages() {
    install_log "Updating sources"
    sudo apt-get update || install_error "Unable to update package list"
}

function install_dependencies() {
    install_log "Installing required packages"
    sudo apt-get -y install lighttpd $php_package git hostapd dnsmasq php7.0-zip php7.0-xml php7.0-mbstring openocd || install_error "Unable to install dependencies"
}

install_raspap
